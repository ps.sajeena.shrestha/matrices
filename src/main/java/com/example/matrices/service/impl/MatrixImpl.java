package com.example.matrices.service.impl;


import com.example.matrices.service.Matrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

public class MatrixImpl implements Matrix {

    private final static Logger logger = LogManager.getLogger(MatrixImpl.class);

    private int row;
    private int column;

    public MatrixImpl() {
        this(0, 0);
    }

    public MatrixImpl(int row, int column) {
        this.row = row;
        this.column = column;
    }

    @Override
    public Integer[][] addition(Integer[][] firstArray, Integer[][] secondArray) {
        this.row = firstArray.length;
        this.column = firstArray[0].length;
        Integer[][] newArray = new Integer[row][column];

        if (!hasEqualRowsAndColumns(firstArray, secondArray)) {
            throw new InputMismatchException("Arrays doesn't have same number of rows and column");
        }

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                newArray[i][j] = firstArray[i][j] + secondArray[i][j];
            }
        }
        logger.info("Sun of Array : {}", Arrays.deepToString(newArray));
        return newArray;

    }

    @Override
    public Integer[][] scalarMultiplication(Integer scalarElement, Integer[][] array) {
        this.row = array.length;
        this.column = array[0].length;
        Integer[][] newArray = new Integer[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                newArray[i][j] = multiply(scalarElement, array[i][j]);
            }
        }

        logger.info("Scalar multiplication: {}", Arrays.deepToString(newArray));
        return newArray;
    }

    @Override
    public Integer[][] transpose(Integer[][] array) {
        this.row = array.length;
        this.column = array[0].length;
        Integer[][] newArray = new Integer[column][row];

        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                newArray[i][j] = array[j][i];
            }
        }

        logger.info("Transportation: {}", Arrays.deepToString(newArray));
        return newArray;
    }

    @Override
    public Integer[][] multiplication(Integer[][] firstArray, Integer[][] secondArray) {
        this.row = firstArray.length;
        this.column = firstArray[0].length;
        Integer[][] newArray = new Integer[row][column];

        if (firstArray[0].length != secondArray.length) {
            throw new InputMismatchException("Number of column of first array is not equal to number of rows of second array");
        }

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                newArray[i][j] = 0;
                for (int k = 0; k < column; k++) {
                    newArray[i][j] += multiply(firstArray[i][k], secondArray[k][j]);
                }
            }
        }

        logger.info("Matrix multiplication : {}", Arrays.deepToString(newArray));
        return newArray;

    }

    @Override
    public Integer[][] getSubMatrix(List<Integer> rows, List<Integer> columns, Integer[][] array) {
        Integer[][] newArray = array;

        if (rows.size() != 0) {
            newArray = removeRows(rows, array);
        }

        if (columns.size() != 0) {
            newArray = removeColumns(columns, newArray);
        }

        logger.info("Sub matrix : {}", Arrays.deepToString(newArray));
        return newArray;
    }

    @Override
    public Integer[][] changeToDiagonal(Integer[][] array) {
        Integer[][] lowerTriangular = changeToLowerTriangular(array);
        return changeToUpperTriangular(lowerTriangular);
    }

    @Override
    public Integer[][] changeToLowerTriangular(Integer[][] array) {
        this.row = array.length;
        this.column = array[0].length;
        Integer[][] newArray = new Integer[row][column];

        checkRowEqualsColumn(row, column);

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (i >= j) {
                    newArray[i][j] = array[i][j];
                } else {
                    newArray[i][j] = 0;
                }
            }
        }

        logger.info("Lower triangular matrix : {}", Arrays.deepToString(newArray));
        return newArray;
    }

    @Override
    public Integer[][] changeToUpperTriangular(Integer[][] array) {
        Integer[][] transposeArray = transpose(array);
        Integer[][] lowerTriangular = changeToLowerTriangular(transposeArray);
        return transpose(lowerTriangular);
    }

    @Override
    public Integer calculateDeterminant(Integer[][] array) {
        int row = array.length;
        int column = array[0].length;
        Integer[][] newArray;
        int result = 0;

        checkRowEqualsColumn(row, column);

        if (row == 1) {
            return array[0][0];
        }

        if (row == 2) {
            return (multiply(array[0][0], array[1][1]) - multiply(array[0][1], array[1][0]));
        }

        for (int i = 0; i < row; i++) {
            newArray = new Integer[row - 1][column - 1];
            for (int j = 1; j < column; j++) {
                for (int k = 0; k < column; k++) {
                    if (k < i) {
                        newArray[j - 1][k] = array[j][k];
                    } else if (k > i) {
                        newArray[j - 1][k - 1] = array[j][k];
                    }
                }
            }

            Integer cofactor = multiply(array[0][i], ((Double) Math.pow(-1, i)).intValue());
            result += multiply(cofactor, calculateDeterminant(newArray));
        }

        logger.info("Total determinant  : {}", result);
        return result;
    }

    private boolean hasEqualRowsAndColumns(Integer[][] firstArray, Integer[][] secondArray) {
        return (firstArray.length == secondArray.length) && (firstArray[0].length == secondArray[0].length);
    }

    private Integer multiply(Integer firstElement, Integer secondElement) {
        return firstElement * secondElement;
    }

    private void checkRowEqualsColumn(int row, int column) {
        if (row != column) {
            throw new InputMismatchException("Number of rows and column should be same");
        }
    }

    private Integer[][] removeRows(List<Integer> removeRows, Integer[][] array) {
        this.row = array.length;
        List<Integer[]> rowsToKeep = new ArrayList<>(row);

        for (int i = 0; i < row; i++) {
            if ((removeRows.contains(i))) {
                continue;
            }
            rowsToKeep.add(array[i]);
        }

        return convertListToArray(rowsToKeep);
    }

    private Integer[][] convertListToArray(List<Integer[]> list) {
        Integer[][] newArray = new Integer[list.size()][];

        for (int i = 0; i < list.size(); i++) {
            newArray[i] = list.get(i);
        }

        return newArray;
    }

    private Integer[][] removeColumns(List<Integer> removeColumns, Integer[][] array) {
        Integer[][] transposeArray = transpose(array);
        Integer[][] newArray = removeRows(removeColumns, transposeArray);

        return transpose(newArray);
    }

}
