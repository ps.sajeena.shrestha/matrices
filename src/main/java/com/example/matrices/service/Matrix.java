package com.example.matrices.service;

import java.util.List;

public interface Matrix {

    Integer[][] addition(Integer[][] firstArray, Integer[][] secondArray);

    Integer[][] scalarMultiplication(Integer scalarElement, Integer[][] array);

    Integer[][] transpose(Integer[][] array);

    Integer[][] multiplication(Integer[][] firstArray, Integer[][] secondArray);

    Integer[][] getSubMatrix(List<Integer> row, List<Integer> column, Integer[][] array);

    Integer[][] changeToDiagonal(Integer[][] array);

    Integer[][] changeToLowerTriangular(Integer[][] array);

    Integer[][] changeToUpperTriangular(Integer[][] array);

    Integer calculateDeterminant(Integer[][] array);

}
