package com.example.matrices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class MatricesApplication {

    public static void main(String[] args) {
        SpringApplication.run(MatricesApplication.class, args);
    }

}
