package com.example.matrices.service;

import com.example.matrices.service.impl.MatrixImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MatrixTest {

    private Matrix matrix;

    @BeforeEach
    public void setUp() {
        matrix = new MatrixImpl();
    }

    @Test
    @DisplayName("Given two array, when addition return sum of two array")
    public void givenTwoArray_whenAddition_thenReturnSumOfTwoArray() {
        Integer[][] firstArray = {{1, 3, 1}, {1, 0, 0}};
        Integer[][] secondArray = {{0, 0, 5}, {7, 5, 0}};
        Integer[][] expectedArray = {{1, 3, 6}, {8, 5, 0}};

        Integer[][] resultArray = matrix.addition(firstArray, secondArray);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Sum of two array should be same as expected");
    }

    @Test
    @DisplayName("Given two array, when addition throws exception")
    public void givenTwoArray_whenAddition_thenThrowException() {
        Integer[][] firstArray = {{1, 3, 1}, {1, 0, 0}};
        Integer[][] secondArray = {{0, 8}, {7, 5}};

        assertThrows(InputMismatchException.class, () -> matrix.addition(firstArray, secondArray),
                "Columns and Rows of both array should be equal");
    }

    @Test
    @DisplayName("Given two array, when scalar multiplication return scalar multiplication of array")
    public void givenScalarElementAndArray_whenScalarMultiplication_thenReturnScalarMultiplicationOfArray() {
        Integer[][] array = {{1, 8, -3}, {4, -2, 5}};
        Integer[][] expectedArray = {{2, 16, -6}, {8, -4, 10}};

        Integer[][] resultArray = matrix.scalarMultiplication(2, array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Scalar Multiplication of array should be same as expected");

    }

    @Test
    @DisplayName("Given an array, when transpose return transportation of array")
    public void givenArray_whenTranspose_thenReturnTransportationOfArray() {
        Integer[][] array = {{1, 2, 3}, {0, -6, 7}};
        Integer[][] expectedArray = {{1, 0}, {2, -6}, {3, 7}};

        Integer[][] resultArray = matrix.transpose(array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Transportation of array should be same as expected");
    }

    @Test
    @DisplayName("Given two array, when multiplication return multiplication of two array")
    public void givenTwoArray_whenMultiplication_thenReturnMultiplicationOfTwoArray() {
        Integer[][] firstArray = {{1, 3, 1}, {1, 7, 2}};
        Integer[][] secondArray = {{2, 3, 4}, {1, 3, 4}, {1, 2, 3}};
        Integer[][] expectedArray = {{6, 14, 19}, {11, 28, 38}};

        Integer[][] resultArray = matrix.multiplication(firstArray, secondArray);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Multiplication two of array should be same as expected");
    }


    @Test
    @DisplayName("Given two array of different size, when multiplication throws exception")
    public void givenTwoArrayDifferentSize_whenMultiplication_thenThrowException() {
        Integer[][] firstArray = {{1, 3, 1}, {1, 0, 0}};
        Integer[][] secondArray = {{2, 3, 4}, {1, 3, 4}};

        assertThrows(InputMismatchException.class, () -> matrix.multiplication(firstArray, secondArray),
                "Number of column of first array should be equal to number of rows of second array ");

    }

    @Test
    @DisplayName("Given an array, when get sub matrix returns sub matrix removing rows")
    public void givenArray_whenGetSubMatrix_thenReturnSubMatrixRemovingRows() {
        Integer[][] array = {{1, 3, 1}, {1, 0, 0}};
        Integer[][] expectedArray = {{1, 3, 1}};

        Integer[] removeRows = {1};
        List<Integer> removeRowList = Arrays.asList(removeRows);

        Integer[][] resultArray = matrix.getSubMatrix(removeRowList, new ArrayList<>(), array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Sub matrix of an array should be same as expected");
    }

    @Test
    @DisplayName("Given an array, when get sub matrix returns sub matrix removing columns")
    public void givenArray_whenGetSubMatrix_thenReturnSubMatrixRemovingColumn() {
        Integer[][] array = {{1, 3, 1}, {1, 0, 0}};
        Integer[][] expectedArray = {{1, 1}, {1, 0}};

        Integer[] removeColumns = {1};
        List<Integer> removeColumnList = Arrays.asList(removeColumns);

        Integer[][] resultArray = matrix.getSubMatrix(new ArrayList<>(), removeColumnList, array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Sub matrix of an array should be same as expected");
    }


    @Test
    @DisplayName("Given an array, when get sub matrix returns sub matrix removing rows and columns")
    public void givenArray_whenGetSubMatrix_thenReturnSubMatrixRemovingRowsAndColumn() {
        Integer[][] array = {{1, 3, 1, 5, 8}, {1, 0, 0, 9, 3}, {1, 2, 4, 8, 7}, {5, 6, 7, 1, 1}};
        Integer[][] expectedArray = {{0, 9, 3}, {6, 1, 1}};

        Integer[] removeRows = {0, 2};
        List<Integer> removeRowList = Arrays.asList(removeRows);

        Integer[] removeColumns = {0, 2};
        List<Integer> removeColumnList = Arrays.asList(removeColumns);

        Integer[][] resultArray = matrix.getSubMatrix(removeRowList, removeColumnList, array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Sub matrix of an array should be same as expected");
    }

    @Test
    @DisplayName("Given an array, when get change to diagonal returns diagonal matrix")
    public void givenArray_whenChangeToDiagonal_thenReturnDiagonalMatrix() {
        Integer[][] array = {{1, 3, 1}, {1, 7, 9}, {1, 2, 4}};
        Integer[][] expectedArray = {{1, 0, 0}, {0, 7, 0}, {0, 0, 4}};

        Integer[][] resultArray = matrix.changeToDiagonal(array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Diagonal matrix of an array should be same as expected");
    }

    @Test
    @DisplayName("Given an array, when get change to diagonal throw exception")
    public void givenArray_whenChangeToDiagonal_thenThrowException() {
        Integer[][] array = {{1, 3}, {1, 7}, {1, 2}};

        assertThrows(InputMismatchException.class, () -> matrix.changeToDiagonal(array),
                "Number of column should be equal to number of rows of an array ");
    }

    @Test
    @DisplayName("Given an array, when get change to lower triangular returns lower triangular matrix")
    public void givenArray_whenChangeToLowerTriangular_thenReturnLowerTriangularMatrix() {
        Integer[][] array = {{1, 3, 1,}, {1, 8, 2}, {1, 2, 4,}};
        Integer[][] expectedArray = {{1, 0, 0}, {1, 8, 0}, {1, 2, 4,}};

        Integer[][] resultArray = matrix.changeToLowerTriangular(array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Lower triangular matrix of an array should be same as expected");
    }

    @Test
    @DisplayName("Given an array, when get change to upper triangular returns upper triangular matrix")
    public void givenArray_whenChangeToUpperTriangular_thenReturnUpperTriangularMatrix() {
        Integer[][] array = {{1, 3, 1,}, {1, 8, 2}, {1, 2, 4,}};
        Integer[][] expectedArray = {{1, 3, 1,}, {0, 8, 2}, {0, 0, 4,}};

        Integer[][] resultArray = matrix.changeToUpperTriangular(array);

        assertEquals(Arrays.deepToString(expectedArray), Arrays.deepToString(resultArray),
                "Upper triangular matrix of an array should be same as expected");
    }

    @Test
    @DisplayName("Given 2x2 array, when calculate determinant returns determinant of matrix")
    public void given2x2Array_whenCalculateDeterminant_thenReturnDeterminant() {
        Integer[][] array = {{3, 4}, {1, 2}};

        assertEquals(2, matrix.calculateDeterminant(array),
                "Determinant of matrix should be two");
    }

    @Test
    @DisplayName("Given 3x3 array, when calculate determinant returns determinant of matrix")
    public void given3x3Array_whenCalculateDeterminant_thenReturnDeterminant() {
        Integer[][] array = {{3, 4, 1}, {1, 2, 5}, {6, 2, 4}};

        assertEquals(88, matrix.calculateDeterminant(array),
                "Determinant of matrix should be eighty eight");
    }

    @Test
    @DisplayName("Given 2x3 array, when calculate determinant throw exception")
    public void given2x3Array_whenCalculateDeterminant_thenThrowException() {
        Integer[][] array = {{3, 4, 5}, {1, 2, 8}};

        assertThrows(InputMismatchException.class, () -> matrix.changeToDiagonal(array),
                "Number of column should be equal to number of rows of an array ");
    }


}
